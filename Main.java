import java.io.*;
import java.math.*;

public class Main {
	public static void main(String[] args) throws IOException {
		DataInputStream d = new DataInputStream(System.in);
		int n = 0;
		String s;
		int c = -1;
		while ((s = d.readLine()) != null) {
			try {
				c = Integer.parseInt(s);
				if(c % 2 == 0) {
					n = c / 2;
				} else if(c % 2 != 0) {
					n = (c + 1) / 2;
				}
				if(c > 86) {
					BigInteger num1;
					num1 = new BigInteger("1");
					System.out.println(efibB(n + 1).subtract(num1));
				}
				else {
					System.out.println(efib(n + 1) - 1);
				}
			} catch(NumberFormatException e) {
				System.out.println("Int型の入力ではありません");
			}
		}
	}


	static long efib(int n) {
		BigInteger P;
		if(n <= 0) {
			return 1;
		} else if(n == 1) {
			return 1;
		} else if(n == 2) {
			return 3;
		} else if(n >= 3) {
		} else {
			return 0;
		}

		long f, fn1, fn2;
		f = 3;
		fn1 = 1;
		for(int i = 3; i <= n; i++) {
			fn2 = fn1;
			fn1 = f;
			f = (3 * fn1 - fn2);
		}
		return f;
	}

	static BigInteger efibB(int n) {
		BigInteger f, fn1, fn2, num3;
		num3 = new BigInteger("3");
		f = new BigInteger("3");
		fn1 = new BigInteger("1");
		for(int i = 3; i <= n; i++) {
			fn2 = fn1;
			fn1 = f;
			f = (num3.multiply(fn1)).subtract(fn2);
		}
		return f;
	}
}
